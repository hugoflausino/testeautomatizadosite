using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using PageObjects;
using System;
using Xunit;

namespace ProjetoSite
{
    public class LoginTest
    {
         private DriverFactory _driverFactory = new DriverFactory();
      
        
        [Theory]
        //Parametros a serem usados no teste
        [InlineData ("hugo.flausino@alock.com.br","Lockey@123","Hugo")]
        public void Login(String email, String password, String name)
        {
            var  telaLogin  = new TelaLogin(_driverFactory);
            telaLogin.displayNavBar(); // verifica se o navbar é carregado
            telaLogin.clickBotaoEntrar();// clica no botao entrar
            telaLogin.displayTelaLogin();//verifica se a tela de login é carregada
            telaLogin.preencherCampoEmail(email);//preenche o campo email
            telaLogin.preencherCampoPassword(password);//preenche o campo senha
            telaLogin.cliqueBotaoEntrar();//clica no botão entrar para fazer login
            telaLogin.verificaUsuarioLogado(name);//verifica se o login foi feito
            telaLogin.Dispose();//encerra o teste
          
        }
    }
} 