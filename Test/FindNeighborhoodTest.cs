using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using PageObjects;
using System;
using Xunit;

namespace ProjetoSite
{
    public class FindNeighborhoodTest
    {
         private DriverFactory _driverFactory = new DriverFactory();
      
        [Theory]
        [InlineData ("SAVASSI - BELO HORIZONTE - MG")]
        public void FindNeighborhood(String neighborhood)
        {
          var telaFindNeighborhood = new TelaHome(_driverFactory);
          telaFindNeighborhood.pesquisaBairro(neighborhood);//Pesquisa pelo bairro
          telaFindNeighborhood.selecionaBairro(neighborhood);//seleciona o bairro na lista dropDown
          telaFindNeighborhood.verificaResultadoPesquisa(neighborhood);
          telaFindNeighborhood.Dispose();//encerra o teste
        }
    }
} 