## Para rodar o projeto

#### Baixar o drive do chrome de acordo com a versão do navegador.
Link: http://chromedriver.chromium.org/downloads.
Ex: meu chrome é a versão 78 entao o driver deve ser 78 também.

Meu driver esta na pasta /usr/share/applications.

No projeto o arquivo DriverFactory.cs contém o caminho para o driver.
É so alterar esse caminho para pasta onde baixou o driver se quiser colocar em pasta diferente da minha.

#### No vscode instalar:

#### Terminal:
* dotnet add package Selenium.WebDriver   
* dotnet add package Selenium.Support 
* dotnet add package Microsoft.NET.Test.Sdk  
* dotnet add package xUnit 
* dotnet add package xunit.runner.visualstudio 

* dotnet restore
* dotnet build

#### Extensão:
* .Net Core Test Explorer
Vai habilitar um item "test" no menu lateral ai você ja sabe como rodar nãó é possivel... é bem intuitivo.


#### Adicionar id nos campos no site
##### Arquivo Dropdown.cs
* linha 2 tag button add id="loggedUser"

##### Arquivo NavBar.cs
* linha 46 botão Entrar add id="btnEntrarNavBar"

##### Arquivo Login.cs
* linha 36 input que recebe o email add id="email-field"
 * linha 66 botão entrar add id="btnEntrarLogin"