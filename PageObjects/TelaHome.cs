using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using ProjetoSite;
using Xunit;

namespace PageObjects
{
    class TelaHome : Page
    {

      public TelaHome(DriverFactory driverFact){
        driverFactory =  driverFact;
        driver = driverFact.GetWebDriver();
      }
      public void pesquisaBairro(String neighborhood){
        driver.FindElement(By.CssSelector(".autocomplete-search:nth-child(2) .v-autocomplete-input")).SendKeys("savassi");
      }

      public void selecionaBairro(String neighborhood){
        var itens = this.driver.FindElements(By.TagName("abbr"));
        Thread.Sleep(2000);
        foreach (var item in itens)
        {
          if(item.Text == neighborhood){
            item.Click();
            break;
          }
        }
      }
    }
}