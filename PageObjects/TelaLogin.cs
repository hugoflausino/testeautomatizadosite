using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using ProjetoSite;
using Xunit;

namespace PageObjects
{
    class TelaLogin : Page
    {


      IWebElement emailLogin;
      IWebElement passwordLogin;
      IWebElement btnEntrarLogin;

      public TelaLogin(DriverFactory driverFact){
        driverFactory =  driverFact;
        driver = driverFact.GetWebDriver();
      }
       

      public void displayTelaLogin(){
        new WebDriverWait(driver, TimeSpan.FromSeconds(2)).Until(drv => drv.FindElement((By.XPath("//h4/span[contains(text(),'ENTRAR')]"))));
      }

      public void preencherCampoEmail(String email){
          emailLogin = driver.FindElement(By.Id("email-field"));
          emailLogin.SendKeys(email);
      }

      public void preencherCampoPassword(String password){
          passwordLogin = driver.FindElement(By.Id("password-field"));
          passwordLogin.SendKeys(password);
      }

      public void cliqueBotaoEntrar(){
        btnEntrarLogin = driver.FindElement(By.Id("btnEntrarLogin"));
        btnEntrarLogin.Click();
      }
    }
}