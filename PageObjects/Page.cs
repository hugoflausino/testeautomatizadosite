using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using ProjetoSite;
using Xunit;

namespace PageObjects
{
  class Page : IDisposable
  {
    public IWebDriver driver { get; set; }
    public DriverFactory driverFactory { get; set; }
    
    public void Dispose()
    {
      this.driverFactory.Close();
    }

    public void clickBotaoEntrar()
    {
      //mapeia o botao entrar
      var button = this.driver.FindElement(By.Id("btnEntrarNavBar"));
      button.Click();
    }

    //Verifica se o navbar aparece
    public void displayNavBar()
    {
      try
      {
        new WebDriverWait(this.driver, TimeSpan.FromSeconds(3)).Until(drv => drv.FindElement((By.ClassName("logo"))));
        // Screenshot image = ((ITakesScreenshot)driver).GetScreenshot();

      }
      catch (System.Exception)
      {

        throw;
      }

    }

    public void verificaUsuarioLogado(String name)
    {
      //mapeia o campo que exibe o nome do usuario logado
      var user =  new WebDriverWait(this.driver, TimeSpan.FromSeconds(2)).Until(drv => drv.FindElement((By.Id("loggedUser"))));
      //compara o campo exibido com o nome do usuario passado como parametro
      Assert.True(name == user.Text);
      Thread.Sleep(2000);//TODO - pode remover depois é só pra ver se esta ok
    }

    public void verificaResultadoPesquisa(String texto){
      var filtro = new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(drv => drv.FindElement((By.CssSelector(".filter-badge-grid"))));
      Assert.True(filtro.Text.Contains(texto));
      Thread.Sleep(2000);//TODO - pode remover depois é só pra ver se esta ok
      //Pode verificar mais coisas
    }

  }
}